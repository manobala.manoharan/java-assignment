package com.ascend;

import java.util.Set;
import java.util.TreeSet;

public class Treeset {
	public static void main(String[] args) {
		
		Set<Integer> set = new TreeSet<>();
		
		set.add(10);
		set.add(20);
		set.add(30);
		set.add(40);
		set.add(50);
		set.add(50);
		
		System.out.println(set);
		
		boolean removes = set.remove(10);
		System.out.println(set);
		
		set.add(10);
		boolean contains = set.contains(10);
		System.out.println(contains);
		
		System.out.println(set);
		int size = set.size();
		System.out.println(size);
		System.out.println("Treeset not allow the dublicate values");
	}

}
