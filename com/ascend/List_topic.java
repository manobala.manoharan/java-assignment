package com.ascend;

import java.util.ArrayList;
import java.util.List;

public class List_topic {
	public static void main(String[] args) {
		
		List<String> list = new ArrayList<>();
		
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		System.out.println(list);

		list.set(4, "mano");
		System.out.println(list);

		list.remove(3);
		System.out.println(list);
		
		boolean contains = list.contains("mano");
		System.out.println(contains);
	}

}
