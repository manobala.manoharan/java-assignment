package com.ascend;

import java.util.LinkedList;
import java.util.List;

public class Linked_list {
	public static void main(String[] args) {
		List<String> l = new LinkedList<>();
		l.add("Mano");
		l.add("Muthu");
		l.add("Nandha");
		l.add("Damu");
		l.add("Anbu");
		l.add("Vinith");
		l.add("Dilip");
		System.out.println(l);
		
		List<Integer> ex = new LinkedList<Integer>();
		ex.add(10);
		ex.add(20);
		ex.add(30);
		ex.add(40);
		ex.add(50);
		ex.add(10);
		ex.add(null);
		
		System.out.println(ex);
		System.out.println("linked list will allow the duplicates and null values");
	}

}
