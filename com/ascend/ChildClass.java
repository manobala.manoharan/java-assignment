package com.ascend;

public class ChildClass extends NumberPyramidPattern {
	
	public void property3() {
		System.out.println("property3 in child class");

	}

	public static void main(String[] args) {
		
		Grantparent g = new Grantparent();// grant parent
		g.property1();

		ParentClass p = new ParentClass();// parent
		p.property2();

		ChildClass c = new ChildClass();// child
		c.property3();

	}

}
