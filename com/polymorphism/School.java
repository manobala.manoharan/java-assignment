package com.polymorphism;

public class School extends College_Details {
	@Override
	public void CollegName() {
		super.CollegName();
	}

	@Override
	public void CollegeLoctions() {
		super.CollegeLoctions();
	}

	public static void main(String[] args) {
		School s = new School();
		s.CollegName();
		s.CollegeLoctions();
	}
}
