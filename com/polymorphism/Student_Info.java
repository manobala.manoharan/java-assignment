package com.polymorphism;

public class Student_Info {   // method overloading
	

	private void sudentID(int age) {
		System.out.println(age);

	}

	private void SudentId(int id, String name) {
		System.out.println(id + "\n" + name);

	}

	private void SudentId(long phone, float Reg) {
		System.out.println(phone + "\n" + Reg);

	}

	public static void main(String[] args) {
		
		
		Student_Info s = new Student_Info();
		s.sudentID(29);
		s.SudentId(55, "Manobala");
		s.SudentId(123456789l, 123987652f);

	}

}
